# Bitbucket Pipelines Pipe: AWS CDK deploy

Deploy your infrastructure as code using [AWS Cloud Development Kit](https://aws.amazon.com/cdk/).

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: valeyko/cdk-deploy:0.0.2
  variables:
    AWS_ACCESS_KEY_ID: '<string>' # Optional if already defined in the context.
    AWS_SECRET_ACCESS_KEY: '<string>' # Optional if already defined in the context.
    AWS_DEFAULT_REGION: '<string>' # Optional if already defined in the context.
    # DEBUG: '<boolean>' # Optional.
```

## Variables

### Basic usage

| Variable                     | Usage                                                |
| ------------------------------- | ---------------------------------------------------- |
| AWS_ACCESS_KEY_ID (**)          |  AWS access key. |
| AWS_SECRET_ACCESS_KEY (**)      |  AWS secret key. |
| AWS_DEFAULT_REGION (**)         |  The AWS region code (us-east-1, us-west-2, etc.) of the region containing the AWS resource(s). For more information, see [Regions and Endpoints][Regions and Endpoints] in the _Amazon Web Services General Reference. |
| DEBUG                           |  Turn on extra debug information. Default: `false`. |
_(*) = required variable. This variable needs to be specified always when using the pipe.
_(**) = required variable. If this variable is configured as a repository, account or environment variable, it doesn't need to be declared in the pipe as it will be taken from the context. It can still be overridden when using the pipe.

## Examples

```yaml
script:
  - pipe: valeyko/cdk-deploy:0.0.2
    variables:
      DEBUG: 'true'
```
