#!/usr/bin/env bash
#
# Required globals:
#   AWS_ACCESS_KEY_ID
#   AWS_SECRET_ACCESS_KEY
#   AWS_DEFAULT_REGION
#
# Optional globals:
#   DEBUG (default: "false")

source "$(dirname "$0")/common.sh"

# required parameters
AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID:?'AWS_ACCESS_KEY_ID missing'}
AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY:?'AWS_SECRET_ACCESS_KEY missing'}
AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION:?'AWS_DEFAULT_REGION missing'}

# default parameters
DEBUG=${DEBUG:="false"}

export AWS_ACCOUNT_ID
AWS_ACCOUNT_ID=$(aws sts get-caller-identity | jq -r .Account)

cdk bootstrap aws://"${AWS_ACCOUNT_ID}"/"${AWS_DEFAULT_REGION}"
cdk deploy --require-approval never
