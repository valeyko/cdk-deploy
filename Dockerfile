FROM adoptopenjdk/openjdk11:alpine-slim

RUN apk --update --no-cache add \
    bash \
    jq \
    npm \
    python3 \
    py-pip \
    && pip install --no-cache-dir awscli \
    && npm install -g aws-cdk \
    && rm -rf /var/cache/apk/* /root/.cache/pip/* \
    && wget --no-verbose -P / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/master/common.sh

COPY pipe /

RUN chmod +x /*.sh

ENTRYPOINT ["/pipe.sh"]
